// Import sheet classes.
import swSagaActorSheet from "./module/sheets/actor-sheet.mjs";
import swSagaActor from "./module/documents/actor.mjs";
import swSagaItem from "./module/documents/item.mjs";
import swSagaItemSheet from "./module/sheets/item-sheet.mjs";
// Import helper/utility classes and constants.
import { swSaga } from "./module/config.mjs";


async function preloadHandlebarsTemplates(){
  const templatePaths = [
    "systems/swSaga/templates/item/partials/components/armor-card.hbs",
    "systems/swSaga/templates/item/partials/components/weapon-card.hbs",
    "systems/swSaga/templates/item/partials/components/force-card.hbs",
    "systems/swSaga/templates/item/partials/components/class-card.hbs"

  ];
  return loadTemplates(templatePaths);
}


/* -------------------------------------------- */
/*  Init Hook                                   */
/* -------------------------------------------- */

Hooks.once('init', async function() {
  console.log('Loading swsaga system')
  
  CONFIG.swSaga = swSaga;
  CONFIG.Actor.documentClass = swSagaActor;
  CONFIG.Item.documentClass = swSagaItem;
  // rollItemMacro
  
  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("swSaga", swSagaActorSheet, { makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("swSaga", swSagaItemSheet, { makeDefault: true });

  preloadHandlebarsTemplates();

  Handlebars.registerHelper('setBonus', (actorObject, skillObject, x, y) =>{
    let armor = 0;
    let talent = 0;
    let feat = 0;
    let equipment = 0;
    if (x === y){
      return actorObject.abilities[x].mod + (skillObject.trained ? 5 : 0) + (Math.floor(actorObject.characterLevel / 2));
    }
    return null;
  })

  Handlebars.registerHelper('hasProperty', (objectToSpy, propertyToFind) =>{
    return objectToSpy.hasOwnProperty(propertyToFind);
  })

  Handlebars.registerHelper('lowercase', (objectToLowerCase) =>{
    return objectToLowerCase.toLowerCase();
  })
  
});

/* -------------------------------------------- */
/*  Hotbar Macros                               */
/* -------------------------------------------- */

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 */
 async function createItemMacro(data, slot) {
  console.log(data)
  if (data.type !== "tem") return;
  if (!("data" in data)) return ui.notifications.warn("You can only create macro buttons for owned Items");
  const item = data.data;

  // Create the macro command
  const command = `game.boilerplate.rollItemMacro("${item.name}");`;
  let macro = game.macros.find(m => (m.name === item.name) && (m.command === command));
  if (!macro) {
    macro = await Macro.create({
      name: item.name,
      type: "script",
      img: item.img,
      command: command,
      flags: { "boilerplate.itemMacro": true }
    });
  }
  game.user.assignHotbarMacro(macro, slot);
  return false;
}


/* -------------------------------------------- */
/*  Ready Hook                                  */
/* -------------------------------------------- */

Hooks.once("ready", async function() {
  // Wait to register hotbar drop hook on ready so that modules could register earlier if they want to
  Hooks.on("hotbarDrop", (bar, data, slot) => createItemMacro(data, slot));
});

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {string} itemName
 * @return {Promise}
 */
function rollItemMacro(itemName) {
  const speaker = ChatMessage.getSpeaker();
  let actor;
  if (speaker.token) actor = game.actors.tokens[speaker.token];
  if (!actor) actor = game.actors.get(speaker.actor);
  const item = actor ? actor.items.find(i => i.name === itemName) : null;
  if (!item) return ui.notifications.warn(`Your controlled Actor does not have an item named ${itemName}`);

  // Trigger the item roll
  return item.roll();
}