export default function defenseCalc(data){
  //calculate reflex defense

  let classBonuses = {fort: 0, ref: 0, will: 0}
  // let reflexBonus = data.characterLevel > data.

  data.ref = Math.floor(10 + data.attributes.characterLevel + ((data.abilities.dex.value - 10) / 2) + classBonuses.ref)
  data.fort = Math.floor(10 + data.attributes.characterLevel + ((data.abilities.con.value - 10) / 2) + classBonuses.fort)
  data.will = Math.floor(10 + data.attributes.characterLevel + ((data.abilities.wis.value - 10) / 2) + classBonuses.will)   
}