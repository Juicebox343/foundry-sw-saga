export async function rollD20(event){
    event.preventDefault();
   
    const element = event.currentTarget;
    const dataset = element.dataset;

    let label = null;
    let roll =  new Roll(`floor(${dataset.roll})`, this.actor.getRollData());
  
    if (dataset.roll) {
      if(dataset.rolltype === 'attack'){
        label = dataset.label ? `${this.actor.data.name} rolls to attack with their ${dataset.label}!` : '';
      }
      if(dataset.rolltype === 'check'){
        label = dataset.label ? `${this.actor.data.name} rolls their ${dataset.label} skill!` : '';
      }
      if(dataset.rolltype === 'stats'){
        label = dataset.label ? `${this.actor.data.name} rolls a straight ${dataset.label} check!` : '';
      }

     
      roll.toMessage({
        user: game.user._id,
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        content: `${roll.total}`,
        flavor: label        
      });
      console.log(roll.formula)
      return roll;
    }
}

export async function rollDamage(event){
  event.preventDefault();
  const element = event.currentTarget;
  const dataset = element.dataset;
  if (dataset.roll) {
    let label = dataset.label ? `${this.actor.name}'s ${dataset.label} hits!` : '';
    let roll = new Roll(`floor(${dataset.roll})`, this.actor.getRollData());
    roll.toMessage({
      speaker: ChatMessage.getSpeaker({ actor: this.actor }),
      flavor: label,
      rollMode: game.settings.get('core', 'rollMode'),
    });
    return roll;
  }
}
